Mac's UI Mod for 7 Days to Die
======
**MacsUIMod** Download: [MacsUIMod.zip](https://bitbucket.org/sgtmac/macsuimod/raw/529f013a30dad0a039ff784ec89d5f0dbb94a52f/MacsUIMod/Binaries/MacsUIMod-a13.7.zip)


## Version
* Version 1.5
* 7DTD Version Alpha 16.1 (b1)

## Features

* HUD Info Panel (Day/Time/Biome Temp) [under compass]
    * Time doesn't 'jump' around
    * Added Biome/Map Temp


* HUDWaterFoodTemp
    * Added the following to bottom left InGameHUD
        * Water (Current/Max)
        * Food (Current/Max)
        * Body Core Temp
        * Biome Temp


* MacsWindowToolbelt
    * Moved the toolbelt up to align properly with HUDLeftStatBars
    * Added numbers below toolbelt to quickly identify toolbelt cell


* HUDMapStats
    * Added the following to the bottom right InGameHUD
        * Date/Time (so that you can see the in-game time while crafting)
            * This may be replaced in the future with player current Lat/Lon, but these variables are not currently available
        * Elevation (for mining purposes)
        * Player Level

## Installation
**It is strongly suggested to make backups of any/all files that are over written** 

0. Download: [MacsUIMod.zip](https://bitbucket.org/sgtmac/macsuimod/raw/529f013a30dad0a039ff784ec89d5f0dbb94a52f/MacsUIMod/Binaries/MacsUIMod-a13.7.zip)
0. Extract MacsUIMod-a13.7.zip
0. Copy MacsUIMod/mod/xui.xml to <7DTD installation folder>/Data/Config/xui.xml
0. Copy MacsUIMod/mod/XUi/windows.xml to <7DTD installation folder>/Data/Config/XUi/windows.xml

**Note:** 
The Xml key <include_window_file> seems to have been either removed or ignored as of Alpha13.7...

**Now xui.xml and ./XUi/windows.xml are both overwritten.  So make backups of both!**

Here is a diff of Mac’sUIMod-xui.xml
![diff xui.xml](https://bytebucket.org/sgtmac/macsuimod/raw/6b8891a09b6440e4da64953806195a27b68f80d4/MacsUIMod/Images/Screen%20Shot%202016-01-29%20at%2011.34.13.png)

**Note:** In order to minimize the impact of modifications to 'windows.xml' my mod is at the top.

## Screenshots
**TODO**
UPDATE SCREENSHOTS
**Windows: 1920x1200**
![](https://bytebucket.org/sgtmac/macsuimod/raw/31020e49d32af732e0069067bbf3a135bdd2fbea/MacsUIMod/Images/2016-01-18_00001.jpg)
![](https://bytebucket.org/sgtmac/macsuimod/raw/31020e49d32af732e0069067bbf3a135bdd2fbea/MacsUIMod/Images/2016-01-18_00002.jpg)
![](https://bytebucket.org/sgtmac/macsuimod/raw/31020e49d32af732e0069067bbf3a135bdd2fbea/MacsUIMod/Images/2016-01-18_00003.jpg)
![](https://bytebucket.org/sgtmac/macsuimod/raw/31020e49d32af732e0069067bbf3a135bdd2fbea/MacsUIMod/Images/2016-01-18_00004.jpg)


