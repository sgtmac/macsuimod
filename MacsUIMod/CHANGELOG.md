# Change Log


## 1.0.0  -  2016-01-18
### Added
- HUD Info Panel (Day/Time/Biome Temp) [under compass]
    - Time doesn't 'jump' around
    - Added Biome/Map Temp


- HUDWaterFoodTemp
    - Added the following to bottom left InGameHUD
        - Water (Current/Max)
        - Food (Current/Max)
        - Body Core Temp
        - Biome Temp


- MacsWindowToolbelt
    - Moved the toolbelt up to align properly with HUDLeftStatBars
    - Added numbers below toolbelt to quickly identify toolbelt cell


- HUDMapStats
    - Added the following to the bottom right InGameHUD
        - Date/Time (so that you can see the in-game time while crafting)
            - This may be replaced in the future with player current Lat/Lon, but these variables are not currently available
        - Elevation (for mining purposes)
        - Player Level


